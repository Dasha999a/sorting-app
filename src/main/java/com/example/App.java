package com.example;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Scanner;

/**
 * App class, instance of which sorts the numbers in ascending order
 *
 */
public class App {
    int[] numbers;
    static final Logger logger = LogManager.getLogger(App.class);
    /**
     * Main constructor that takes in the quantity of numbers, that will be provided by user
     * @param quantity the quantity of numbers
     */
    public App(int quantity) {
        if (quantity == 0 || quantity > 10) throw new IllegalArgumentException();
        numbers = new int[quantity];
    }

    /**
     * enterNumbers method, that takes no arguments. Reads integers from System.in and saves them to numbers
     * array
     */
    public void enterNumbers() {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        while (n < numbers.length) {
            System.out.print("Number " + (n+1) + ": ");
            numbers[n++] = sc.nextInt();
            sc.nextLine();
        }
    }

    /**
     * sortNumbers method, that takes no arguments. Sorts the numbers in ascending order
     */
    public void sortNumbers() {
        Arrays.sort(numbers);
        for (int num : numbers) System.out.print(num + " ");
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();
        logger.info("Setup logger basic configuration");
    }
}
