package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * Parameterized unit test
 */
@RunWith(Parameterized.class)
public class AppTest {
    private int quantity;

    /**
     * Constructor that takes in quantity of numbers
     * @param quantity quantity of numbers
     */
    public AppTest(int quantity) {
        this.quantity = quantity;
    }

    /**
     * data method that provides number quantity as a parameter for a constructor
     * @return
     */
    @Parameterized.Parameters
    public static Collection<Integer> data() {
        return Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9);
    }


    /**
     * test that tests sortNumbers method in App class, by generating the necessary quantity of numbers
     * runs 8 times
     */
    @Test
    public void testSortNumbersWithGoodQuantity() {
        App app = new App(quantity);
        int n = 0;
        int[] appNumbers = new int[quantity];
        for (; n < quantity; n++) {
            appNumbers[n] = (int) (Math.random() * 1000);
        }
        app.numbers = appNumbers;
        Arrays.sort(appNumbers);
        int[] expected = appNumbers;
        app.sortNumbers();
        assertArrayEquals(expected, app.numbers);
    }
}
