package com.example;
import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.*;

/**
 * Unit test that tests corner cases
 */
public class AppTestCornerCases {

    /**
     * Test that tests if the exception is thrown when the quantity of numbers 0
     */
    @Test
    public void testCornerCaseWhenQuantityIs0() {
        try {
            new App(0);
        } catch (IllegalArgumentException expectedException) {
            return;
        }

        // If no exception is thrown, fail the test
        Assert.fail("Expected exception was not thrown.");
    }


    /**
     * Test that tests the sortNumbers method when the quantity of numbers 1
     */
    @Test
    public void testCornerCaseWhenQuantityIs1() {
        App app = new App(1);
        int[] expected = {5};
        app.numbers = expected;
        Arrays.sort(expected);
        app.sortNumbers();
        assertArrayEquals(expected, app.numbers);
    }


    /**
     * Test that tests the sortNumbers method when the quantity of numbers 10
     */
    @Test
    public void testCornerCaseWhenQuantityIs10() {
        App app = new App(10);
        int n = 0;
        int[] appNumbers = new int[10];
        for (; n < 10; n++) {
            appNumbers[n] = (int) (Math.random() * 1000);
        }
        app.numbers = appNumbers;
        Arrays.sort(appNumbers);
        int[] expected = appNumbers;
        app.sortNumbers();
        assertArrayEquals(expected, app.numbers);
    }


    /**
     * Test that tests if the exception is thrown when the quantity of numbers 11
     */
    @Test
    public void testCornerCaseWhenQuantityIs11() {
        try {
            new App(11);
        } catch (IllegalArgumentException expectedException) {
            return;
        }

        // If no exception is thrown, fail the test
        Assert.fail("Expected exception was not thrown.");
    }
}
